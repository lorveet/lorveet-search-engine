<?php 

    ini_set("date.timezone", "Africa/Lagos");
    date_default_timezone_set(
        "Africa/Lagos"
    );

    include $_SERVER['DOCUMENT_ROOT']."/lorveet/controller/jobs.class.php";
    include $_SERVER['DOCUMENT_ROOT']."/lorveet/controller/connection.php";
    include $_SERVER['DOCUMENT_ROOT']."/lorveet/controller/genie.class.php";

    // $xml = file_get_contents("https://ngcareers.com/xmlfeed.xml");
    // $jobs = new SimpleXMLElement($xml);

    $db = new DBConnection();

    $genie = new genie();

    // $totalJobs = count($jobs->jobs->job);

    // // first we insert new jobs into the database
    // for($i = 0; $i < count($jobs->jobs->job); $i++){
    //     $title = $jobs->jobs->job[$i]->title;
    //     $url = $jobs->jobs->job[$i]->url;
    //     $salary = $jobs->jobs->job[$i]->salary;
    //     $category = $jobs->jobs->job[$i]->category;
    //     $company = $jobs->jobs->job[$i]->company;
    //     $date = date("Y-m-d", strtotime($jobs->jobs->job[$i]->date));
    //     $experience = $jobs->jobs->job[$i]->experience;
    //     $location = $jobs->jobs->job[$i]->state;
    //     $type = $jobs->jobs->job[$i]->jobtype;
    //     $specialization = $jobs->jobs->job[$i]->category;
    //     $description = $jobs->jobs->job[$i]->description;
    //     $experience = $jobs->jobs->job[$i]->experience." experience";

    //     $queryStatus[] = $db->execQuery("INSERT INTO jobs(job_title, company, job_link, job_location, 
    //                                 job_industry, date_posted, job_requirements, 
    //                                 job_time, salary) 
    //                     VALUES ('$title', '$company', '$url', '$location', '$specialization',
    //                             '$date', '$experience', '$type', '$salary')");
    // }

    // $emailTemplate = file_get_contents("../../html/cron-notification.html");

    // $emailTemplate = str_replace("{job-source}", "https://ngcareers.com/xmlfeed.xml", $emailTemplate);
    // $emailTemplate = str_replace("{num-jobs}", $totalJobs, $emailTemplate);
    // $emailTemplate = str_replace("{date-time}", date("Y-m-d"), $emailTemplate);
    // $emailTemplate = str_replace("{status}", $statusMsg, $emailTemplate);

    //once we are done with inserting new jobs, we send the jobs to our beloved subscribers

    //set today's date since our host is in a different timezone

    $today = date("Y-m-d", strtotime("yesterday"));
    $mailContent = "";
    //fetch all user details
    $query = $db->execQuery("SELECT * FROM user WHERE email = 'contact@lorveet.com' ");
    
    while($userDetails = $query->fetch_assoc()):
        $email = $userDetails['email'];
        $industry = $userDetails['industry'];
        $lastname = $userDetails['lastname'];

        //we are going to break the concantenated interests into an array
        $industryArray = explode("+", rtrim($industry, "+"));

        //jobs query
        $jobQuery = "SELECT * FROM jobs";
        $jobQuery .= " WHERE (";
        foreach($industryArray as $industries){
            $jobQuery .= " job_industry LIKE '%$industries%' OR ";
        }
        $jobQuery = rtrim($jobQuery, " OR ");
        $jobQuery .= " ) ";
        $jobQuery .= "AND date_posted = '$today' LIMIT 10";

        $queryJob = $db->execQuery($jobQuery);

        //for each user, create an html with their jobs of interest
        while($results = $queryJob->fetch_assoc()):
            $title = $results['job_title'];
            $company = $results['company'];
            $experience = $results['job_requirements'];
            $url = $results['job_link'];
            $location = $results['job_location'];
            $type = $results['job_time'];
            $date = ucfirst($results['date_posted']);
            $specialization = $results['job_industry'];
            $job_id = $results['sn'];

                $jobNode = '<div>
                                <h3 style="margin-bottom: 5px;">
                                    <a href="https://www.lorveet.com/search?clk={job-link}" style="color: rgb(0, 4, 126);text-decoration: none;">{job-title} at {company}</a>
                                </h3>
                                <b>Minimum requirements:</b> {requirements}
                                <b>Job type:</b> {job-type}
                                <span style="color: #737373; font-weight: 600;width: 100%;font-size: 14px;display: block;">Industry: <a href="https://www.lorveet.com/search?q={industry}&src=industry_email_feed">{industry}</a></span>
                                <span style="color: #737373; font-weight: 600;width: 100%;font-size: 14px;display: block;">Location: <a href="https://www.lorveet.com/search?q={location}&src=location_email_feed">{location}</a></span>
                                <span style="color: #737373; font-weight: 600;width: 100%;font-size: 14px;display: block;">Date: {date}</span>                   
                            </div>';
        
                $jobNode = str_replace("{job-title}", $title, $jobNode);
                $jobNode = str_replace("{company}", $company, $jobNode);
                $jobNode = str_replace("{requirements}", $experience, $jobNode);
                $jobNode = str_replace("{job-type}", ucfirst($type), $jobNode);
                $jobNode = str_replace("{industry}", $specialization, $jobNode);
                $jobNode = str_replace("{location}", $location, $jobNode);
                $jobNode = str_replace("{date}", $date, $jobNode);
                $jobNode = str_replace("{job-link}", $job_id, $jobNode);
        
                $mailContent .= $jobNode;

        endwhile;

        if(!empty($mailContent)){
            //subscribers email
            $emailTemplate_too = file_get_contents("../../html/job-listings.html");

            //subscribers content
            $emailTemplate_too = str_replace("{content}", $mailContent, $emailTemplate_too);
            //replace name of the user
            $emailTemplate_too = str_replace("{name}", $lastname, $emailTemplate_too);

            echo $emailTemplate_too;

            // $genie->EmailNotify($email, "$title at $company and other jobs are available for you today via Lorveet", $emailTemplate_too);
        }

        //empty the variables
        $mailContent = "";
        $jobNode = "";
    endwhile;

    // $genie->EmailNotify("admin@lorveet.com, contact@lorveet.com", "Cron Job run successful", $emailTemplate);
?>
