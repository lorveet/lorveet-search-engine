<?php 
    ini_set("date.timezone", "Africa/Lagos");
    include $_SERVER['DOCUMENT_ROOT']."/lorveet/vendor/autoload.php";

    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();

    class DBConnection{
        private $host;
        private $dbname;
        private $user;
        private $pass;
        private $connection;
        
        public function __construct(){

            $connect = new mysqli(getenv('host'), getenv('user'), getenv('pass'), getenv('dbname'));

            if(mysqli_connect_error()){
                die("Failed to connect to database ". mysqli_connect_errno());
            }

            $this->connection = $connect;
        }

        public function getset(){
            return false;
        }

        public function execQuery($query){
            $didQuery = $this->connection->query($query);
            if($didQuery){
                return $didQuery;
            }else{
                return $this->connection->error;
            }
        }

        public function sanitize($string){
            return trim(strip_tags($this->connection->escape_string($string)));
        }

        public function dateAgo($date, $daysAgo = false){
            
            $cdate = mktime(0, 0, 0, date('m', strtotime($date)), date('d', strtotime($date)), date('Y', strtotime($date)));
            
            $today = time();
            
            if($daysAgo === true){
                $difference = $today - $cdate;
            }else{
                $difference = $cdate - $today;
            }
            
            if ($difference < 0) { $difference = 0; }
            
            $daysRemaining = floor($difference/60/60/24);
            
            return $daysRemaining;
        }

        public function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' ){
            $datetime1 = date_create($date_1);
            $datetime2 = date_create($date_2);
            
            $interval = date_diff($datetime1, $datetime2);
            
            return $interval->format($differenceFormat);
            
        }
    }