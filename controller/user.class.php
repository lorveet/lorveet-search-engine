<?php 
    require_once __DIR__."/connection.php";

    class User extends DBConnection{
        private $id;
        private $fname;
        private $lname;
        private $email;
        private $phone;
        private $address;
        private $interests;

        private function now(){
            return date("Y-m-d h:ia");
        }

        public function register($array){
            
            foreach($array as $key => $value){
                if($key === "email"){
                    $givenMail = $value;
                }
                $columns[] = $key;
                $values[] = $this->sanitize($value);
            }

            //email check
            if(isset($givenMail)){
                $query = "SELECT email FROM user WHERE email = '$givenMail'";
                    if($this->execQuery($query)){
                        if($this->execQuery($query)->num_rows > 0){
                            return "This user already exist";
                        }
                    }
            }

            $columns[] = "user_id";
            $values[] = $this->newUserId();

            //add date
            $columns[] = "date_created";
            $values[] = $this->now();

            $column = implode(", ", $columns);
            $value = '"'.implode('", "', $values).'"';

            $query = "INSERT INTO user ($column) VALUES ($value)";

            $didQuery = $this->execQuery($query);

            if(is_integer($didQuery) && $didQuery == 1){
                return true;
            }else{
                return $didQuery;
            }
        }

        public function getUserId($email){

            $query = "SELECT user_id FROM user WHERE email = '$email'";

            $didQuery = $this->execQuery($query);
            
            if($didQuery){

                $username = $didQuery->fetch_assoc();
                
                $userid = $username['user_id'];
                
                return $userid;
            }else{
                return "Invalid user.";
            }
        }

        public function setUserId($userid){
            $this->id = $userid;
        }

        public function getUsername($email){
            $email = $this->sanitize($email);

            $query = "SELECT * FROM user WHERE email = '$email' ";

            $didQuery = $this->execQuery($query);

            if($didQuery){
                if($didQuery->num_rows > 0){
                    $username = $didQuery->fetch_assoc();
                    $this->setUserId($username['user_id']);
                    $user_name = $username['firstname']." ".$username['lastname'];
                    return $user_name;
                }else{
                    return "Invalid user.";
                }
            }else{
                return $didQuery;
            }
        }

        public function login($email, $pass){
            $query = "SELECT password FROM user WHERE email = '$email'";
            $didQuery = $this->execQuery($query);
                if($didQuery){
                    if($didQuery->num_rows > 0){
                        while($dbpass = $didQuery->fetch_assoc()):
                            $password = $dbpass['password'];
                        endwhile;

                        if(password_verify($pass, $password)){
                            $_SESSION['loggedIn'] = $pass;
                            return true;
                        }else{
                            return "Email or password is invalid.";
                        }
                    }else{
                        return "The details you have entered do not match our record.";
                    }
                }else{
                    return $didQuery;
                }
        }

        public function createNewHash($userId, $type){
            //md5 hash
            $hash = md5(time() + rand(time(), mktime()+8600));

            $now = $this->now();

            //insert hash into database
            $query = "INSERT INTO hash(type, hash, user_id, dateCreated) VALUES('$type', '$hash', '$userId', '$now')";

            $savedHash = $this->execQuery($query);

            if($savedHash){
                return $hash;
            }else{
                return false;
            }
        }

        public function sendEmailValidation($type, $mailTitle, $userId, $content){

            /**
             * @type - type of mail to be sent [0 => validation/notification/password-reset, 1 => admin]
             * @userid - id of the receipent
             * $content (array) - key-value pair of content
             */

            //1. Fetch user email
            $query = "SELECT email FROM user WHERE user_id = '$userId'";

            $didQuery = $this->execQuery($query);

            if($didQuery){
                $queryArray = $didQuery->fetch_assoc();
                $email = $queryArray['email'];
            }
            
            //2. load mail template depending on $type
            switch($type){
                case '0':
                    $template = file_get_contents("html/email-alerts.html");
                    break;
                case '1':
                    $template = file_get_contents("html/cron-notification.html");    
                    break;
                default:
                    $template = file_get_contents("html/email-alerts.html");
                    break;
            }

            //3. Add $content to template
            foreach($content as $key => $value){
                $template = str_replace($key, $value, $template);
            }

            //4. Send mail to receipent
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: Lorveet Genie <genie@lorveet.com>' . "\r\n";
            $headers .= 'Cc: contact@lorveet.com' . "\r\n";
                                    
            //5. Store email specs in db before sending
            $now = date("Y-m-d h:ia");
            $query = "INSERT INTO email(user_id, content, dateCreated, type) VALUES('$userId', '$template', '$now', '$type')";
            $didQuery = $this->execQuery($query);

            $sentMail = mail($email, $mailTitle, $template, $headers);

            if($sentMail){
                return true;
            }else{
                return false;
            }
        
        }

        public function validateAccount($email, $pass, $c_pass, $type = "update"){

                //validate and hash password
                if($pass != $c_pass) return "Your passwords do not match.";
                if(strlen($pass) < 8) return "Password must be 8 characters or more.";
                $cost = [
                    "cost" => 12
                ];
                $password = password_hash($pass, PASSWORD_BCRYPT, $cost);

                if($type == "update"){
                    /**
                     * We don't want a situation where people change their password
                     * without getting an hash (from the $_GET['activate']). Whatever method
                     * that calls this method has to verify the hash with @method verify_hash()
                     * before calling this method. I could update this method to verify the 
                     * hash itself in future
                     */
                    $query = "UPDATE user SET account_status = 'active', password = '$password' WHERE email = '$email'";
                    $didUpdate = $this->execQuery($query);
                    if($didUpdate == 1){
                        return true;
                    }else{
                        return $didUpdate;
                    }
                }elseif($type == "new"){
                    /**
                     * If anyone calls this method without the `hash` from $_GET['activate]
                     * then they must be a new user and not have previously set password
                     */
                    $query_2 = "SELECT * FROM user WHERE email = '$email' ";
                    $didQuery = $this->execQuery($query_2);
                    if($didQuery){
                        $results = $didQuery->fetch_assoc();
                        $dbpass = $results['password'];
                        if(empty($dbpass)){
                            $update_query = "UPDATE user SET account_status = 'active', password = '$password' WHERE email = '$email'";
                            $didUpdate = $this->execQuery($update_query);
                            if($didUpdate == 1){
                                return true;
                            }else{
                                return $didUpdate;
                            }
                        }else{
                            return "Sorry but this action is invalid. If you are having problems signing in to your account, 
                                        kindly use the feedback button at the bottom of the page to get support.";
                        }
                        
                    }else{
                        print_r($didQuery);
                        return "Sorry but this action is invalid. If you are having problems signing in to your account, 
                                        kindly use the feedback button at the bottom of the page to get support.";
                    }
                }

        }

        public function newUserId(){
            return substr(md5(time()."|\/||)\/"), 0, 7);
        }

        public function feedback($email, $text, $mood, $url){
            //prepare values for db
            $email = $this->sanitize($email);
            $text = $this->sanitize($text);
            $mood = $this->sanitize($mood);
            $ip = $_SERVER['REMOTE_ADDR'];
            $browserId = $_COOKIE['_genie'];
            $now = date("Y-m-d h:ia");
            $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';

            $query = "INSERT INTO feedback(email, text, ip, page, browser, mood, timestamp) 
                        VALUES ('$email', '$text', '$ip', '$url', '$browserId', '$mood', '$now')";

            $didQuery = $this->execQuery($query);

            if($didQuery){
                //send email
                return true;
            }else{
                return $didQuery;
            }
        }

        public function reset_password($userId, $content, $mailTitle = "Reset Password - Lorveet Genie", $type = "0"){
            //1. send mail
            $sent = $this->sendEmailValidation($type, $mailTitle, $userId, $content);

            if($sent){
                return true;
            }else{
                return false;
            }
        }

        public function verify_email($email){
            $query = "SELECT * FROM user WHERE email = '$email'";
            $didQuery = $this->execQuery($query);
            if($didQuery && $didQuery->num_rows > 0){
                return true;
            }else{
                return false;
            }
        }

        public function getFirstname($userId){
            /**
             * @return bool (false) if invalid user id
             * @return $firstname (text) if correct
             */
            $query = "SELECT firstname FROM user WHERE user_id = '$userId'";
            $didQuery = $this->execQuery($query);
            if($didQuery){
                $response = $didQuery->fetch_assoc();
                $firstname = $response['firstname'];
                return $firstname;
            }else{
                return false;
            }
        }

        public function verify_hash($hash){
            $query = "SELECT u.email, h.hash, h.user_id, h.dateCreated FROM user u, hash h 
                        WHERE h.user_id = u.user_id AND h.hash = '$hash' ";
            
            $didQuery = $this->execQuery($query);

            if($didQuery){
                $response = $didQuery->fetch_assoc();
                $email = $response['email'];

                $dateSent = $response['dateCreated'];

                $today = time();

                $timeAgo = strtotime(date("Y-m-d H:i:s")) - strtotime($dateSent);
            
                if($timeAgo <= 900){
                    $result['email'] = $email;
                    $result['status'] = true;
                    return $result;
                }else{
                    return "The link you followed is expired or invalid.";
                }
            }
        }

        public function authLinkedin($json){

            $data = json_decode($json);

            $email = $this->sanitize($data->emailAddress);
            $fname = $this->sanitize($data->firstName);
            $lname = $this->sanitize($data->lastName);
            $occupation = $this->sanitize($data->headline);
            $industry = $this->sanitize($data->industry);
            $image = $this->sanitize($data->pictureUrls->values[0]);
            // $bio = $this->sanitize($data->summary);
            $bio = "Bio data";
            $userid = $this->newUserId();
            $today = $this->now();
            $username = strtolower($this->autoGeneratedUsername($fname, $lname));

            $query = "INSERT INTO user(email, firstname, lastname, user_id, industry, bio, picture, username, date_created, job_title, user_type)
                        VALUES ('$email', '$fname', '$lname', '$userid', '$industry', '$bio', '$image', '$username', '$today', '$occupation', 'people')";

            $didQuery = $this->execQuery($query);

            if($didQuery){
                $response = array("email" => $email, "status" => "success", "username" => $username, "message" => $didQuery);
                return $response;
            }else{
                return $didQuery;
            }

        }

        /***************************************************************/
        /***************************************************************/
        /**********************BEGIN PEOPLE*****************************/
        /***************************************************************/
        /***************************************************************/

        public function getProfile($user){
            /**
             * @var $user - username or email of the user
             */
            $query = "SELECT * FROM user WHERE (email = '$user' OR username = '$user') AND user_type = 'people'";
            $didQuery = $this->execQuery($query);

            if($didQuery->num_rows == 1){
                $response = $didQuery->fetch_assoc();
                return $response;
            }else{
                return "This user doesn't exist in the people's category.";
            }
        }

        public function print($something, $alt){
            echo (empty($something)) ? $alt : $something;
        }

        public function createUsername($username, $email){
            $query = "SELECT * FROM user WHERE username = '$username'";
            $didQuery = $this->execQuery($query);

            if($didQuery->num_rows == 1){
                return "This username has been selected try another with some numbers, underscore or dot. ".$didQuery->num_rows;
            }else{
                $createQuery = "UPDATE user SET username = '$username' WHERE email = '$email' ";
                $createdUser = $this->execQuery($createQuery);

                if($createdUser){
                    $response['username'] = $username;
                    $response['message'] = "Success! Your username is $username.";
                    return $response;
                }else{
                    echo $createdUser;
                }
            }
        }

        public function checkUsername($username_intend){
            $didQuery = $this->execQuery("SELECT * FROM user WHERE username = '$username_intend' ");

            if($didQuery->num_rows > 0){
                $username_intend = $username_intend.rand(007, 700);
                $this->checkUsername($username_intend);
            }else{
                return $username_intend;
            }
        }

        public function autoGeneratedUsername($fname, $lname){
            /***
             * We want all usernames to be first name and last name.
             * in cases where they already exists, we append random numbers at the 
             */
            $username_intend = $fname.$lname;

            $checkUsername = $this->checkUsername($username_intend);

            if(!$checkUsername){
                return "An error occured. Please try again later.";
            }else{
                return $checkUsername;
            }
        }

        public function getRandomGenies(){
            /**
             * Method to fetch genies for the home page
             */
            $query = "SELECT * FROM user WHERE user_type = 'people' AND bio != '' AND picture != '' AND job_title != '' LIMIT 3";
            $didQuery = $this->execQuery($query);

            if($didQuery){
                while($response = $didQuery->fetch_assoc()):
                    $result[] = $response;
                endwhile;

                return $result;
            }else{
                return $didQuery;
            }
        }

        public function updatePeoplePersonal($fname, $lname, $username, $phone, $dob, $email){
            /****
             * This method updates the personal information of a genie
             */

             $query = "UPDATE user SET firstname = '$fname', lastname = '$lname', username = '$username', phone = '$phone', dob = '$dob' WHERE email = '$email' ";

             $didUpdate = $this->execQuery($query);

             if($didUpdate){
                 return true;
             }else{
                 return $didUpdate;
             }
        }

        public function updatePeopleBio($bio, $email){
            /**
             * This method updates the bio of genie
             */

             if(strlen($bio) > 300){
                 return "Bio cannot be more than 300 characters.";
             }else{
                 $query = "UPDATE user SET bio = '$bio' WHERE email = '$email'";
                 $didQuery = $this->execQuery($query);

                 if($didQuery){
                     return true;
                 }else{
                     return $didQuery;
                 }
             }
        }

        public function updatePeopleLinks($links, $email){
            /**
             * this method updates and creates a social link for users where it does not exist
             */
            $userid = $this->getUserId($email);
            
            //check if the socials exist
            $query = "SELECT * FROM socials WHERE user_id = '$userid' ";
            $didQuery = $this->execQuery($query);
            if($didQuery->num_rows > 0){
                //run update query
                foreach($links as $link):
                    $url = $this->sanitize($link['url']);
                    $social = $this->sanitize($link['social']);

                    $didUpdate = $this->execQuery("UPDATE socials SET social = '$social' WHERE user_id = '$userid' AND url = '$url' ");
                    if(!$didUpdate){
                        $response[] = $didUpdate;
                    }
                endforeach;

                if(!isset($response)){
                    return true;
                }else{
                    return $response;
                }
            }else{
                //run insert query
                foreach($links as $link):
                    $url = $this->sanitize($link['url']);
                    $social = $this->sanitize($link['social']);

                    $didUpdate = $this->execQuery("INSERT INTO socials(user_id, social, url) VALUE('$userid', '$social', '$url') ");
                    if(!$didUpdate){
                        $response[] = $didUpdate;
                    }
                endforeach;
                if(!isset($response)){
                    return true;
                }else{
                    return $response;
                }
            }
        }
    }