<?php 
    /**
     * This is a class for every automated action 
     */

    include $_SERVER['DOCUMENT_ROOT']."/lorveet/vendor/autoload.php";

    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();

    class genie{

        public function EmailNotify($subj, $content, $to){

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: Lorveet Genie <genie@lorveet.com>' . "\r\n";

            mail($to, $subj, $content, $headers);
        }  

        public function asset($url){
            /**
             * returns the absolute path for an asset
             */
            if(stripos($url, "http") === FALSE || stripos($url, "https") === FALSE){
                return getenv('local_url').$url;
            }else{
                return $url;
            }
            
        }

        public function csrf(){
            $hash = hash('md5' ,time().rand(1329, 5999).md5("genie"));

            if(!isset($_COOKIE['csrf'])){
                setcookie('csrf', $hash, time()+60*5);
                return $hash;
            }else{
                return $_COOKIE['csrf'];
            }
        }

        public function getUrl(){
            return getenv('local_url');
        }
    
    }
?>