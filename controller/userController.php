<?php

// include "genie.class.php";
include "user.class.php";

$user = new User();

if(isset($_POST['submit'])){
    //create account 

    $fname = $_POST['firstname'];
    $fname = $_POST['lastname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $industry = $_POST['industry'];

    array_pop($_POST);

    foreach($_POST as $key => $value){
        if( empty($_POST[$key]) && $key != "submit"){
            $error['type'] = "error";
            $error['message'][] = "The ".ucfirst($key)." field is required.";
        }
    }

    if(empty($error)){
        $registered = $user->register($_POST);

        if(!is_bool($registered) && $registered !== true){
            $error['type'] = "error";
            $error['message'][] = $registered;
        }else{
            $error['type'] = "success";
            header("location: ?setpassword&user=$email");
        }
    }
}elseif(isset($_POST['submitLogin'])){
    $email = $_POST['email'];
    $password = $_POST['password'];

    $didLogin = $user->login($email, $password);

    if(is_bool($didLogin) && $didLogin === true){
        $_SESSION['loggedIn'] = $email;
        header("location: /");
    }elseif($didLogin === false){
        $error[] = "An unknown error occured";
    }else{
        $error[] = $didLogin;
    }
}elseif(isset($_POST['submitActivateAccount'])){
        //set password for new accounts
        if(!isset($_GET['user'])){
            $error['type'] = "error";
            $error['message'][] = "Sorry, this is an invalid request. 
                                    If you are having troubles creating an account, 
                                    use the feedback for at the bottom to get support.";
            return false;
        }

        $email = $_GET['user'];
        $pass = $_POST['password'];
        $c_pass = $_POST['cpassword'];
        $didValidate = $user->validateAccount($email, $pass, $c_pass, "new");

        if(is_bool($didValidate) && $didValidate === true){
            $error['type'] = "success";
            $error['message'][] = "Account successfully activated. You will be redirected now.";
            $_SESSION['loggedIn'] = $_GET['user'];
            if(isset($_GET['step'])){
                header("location: https://www.lorveet.com/genies/people/profile/$email?status=welcome");
            }else{
                header("refresh:3; url=/");
            }
        }else{
            $error['type'] = "error";
            $error['message'][] = $didValidate;
        }
}elseif(isset($_POST['reset-password'])){
    //process reset password request
    //email from form
    $email = $_POST['email'];

    //check if email exists
    if(!$user->verify_email($email)){
        $error['type'] = "error";
        $error['message'][] = "Invalid account. The email you entered doesn't match our record.";
    }else{
        //set user id
        $userId = $user->getUserId($email);

        //create content
        $content = array(
            "{alert-title}" => "Password reset request",
            "{salutation}" => "Dear ".$user->getFirstname($userId),
            "{big-title}" => "Here is a link to reset your password. This link expires in 15 minutes.",
            "{message}" => "Click this link to reset your password: "."https://www.lorveet.com/reset-password?activate=".$user->createNewHash($userId, '0')
        );

        $didReset = $user->reset_password($userId, $content);

        $error['type'] = "success";
        $error['message'][] = "We have sent a password reset link to your email. Click on the link to reset your password. This link expires in 15 minutes.";
    }
}elseif(isset($_POST['reactivateAccount'])){
    ini_set("date.timezone", "Africa/Lagos");
    if(isset($_GET['activate'])){

        $pass = $_POST['password'];
        $c_pass = $_POST['cpassword'];

        $getHash = $_GET['activate'];
        $isVerified =  $user->verify_hash($getHash);

        if(is_array($isVerified) && $isVerified['status'] === true){

            $didValidate = $user->validateAccount($isVerified['email'], $pass, $c_pass);

            if(is_bool($didValidate) && $didValidate === true){
                $error['type'] = "success";
                $error['message'][] = "You have successfully updated your password. You will now be redirected to login.";

                //redirect
                header("refresh:3; url=/login");
            }else{
                $error['type'] = "error";
                $error['message'][] = $didValidate;
            }
        }else{
            $error['type'] = "error";
            $error['message'][] = $isVerified;
        }
    }
}elseif(isset($_POST['createUsername']) && isset($_GET['profile'])){
    /**
     * Called with AJAX
     */
    $username = $_POST['createUsername'];
    $user_email = $_GET['profile'];
    $didUsername = $user->createUsername($username, $user_email);

    if(is_array($didUsername)){
        echo $didUsername['message'];
    }else{
        echo $didUsername;
    }
}elseif(isset($_POST['updatePersonalInfo'])){
    $fname = $_POST['firstname'];
    $lname = $_POST['lastname'];
    $username = $_POST['username'];
    $phone = $_POST['phone'];
    $dob = $_POST['dob'];
    $email = $_SESSION['loggedIn'];
    $didUpdate = $user->updatePeoplePersonal($fname, $lname, $username, $phone, $dob, $email);

    if($didUpdate){
        $message['header'] = "Settings saved successfully.";
        $message['body'] = "Profile updated successfully";
    }else{
        $message['header'] = "An error occured.";
        $message['body'] = $didUpdate;
    }
}elseif(isset($_POST['updateBio'])){
    $bio = $_POST['bio'];
    $email = $_SESSION['loggedIn'];

    $didUpdate = $user->updatePeopleBio($bio, $email);

    if($didUpdate){
        $message['header'] = "Settings saved successfully.";
        $message['body'] = "Profile updated successfully";
    }else{
        $message['header'] = "An error occured.";
        $message['body'] = $didUpdate;
    }
}elseif(isset($_POST['updateLinks'])){
    $fcbk = $_POST['facebook'];
    $insta = $_POST['instagram'];
    $linkedin = $_POST['linkedin'];
    $email = $_SESSION['loggedIn'];

    $links = array(
        "url" => "https://www.facebook.com",
        "social" => $fcbk,
        "url" => "https://www.instagram.com",
        "social" => $insta,
        "url" => "https://www.linkedin.com",
        "social" => $linkedin,
    );
    
    $didUpdate = $user->updatePeopleLinks($links, $email);

    if($didUpdate){
        $message['header'] = "Settings saved successfully.";
        $message['body'] = "Profile updated successfully";
    }else{
        $message['header'] = "An error occured.";
        $message['body'] = $didUpdate;
    }
}