function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
      return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
      return uri + separator + key + "=" + value;
    }
}

function setIndustry(value){
    const industryField = document.querySelector("#industry");

    industryField.value += value+"+";
}

function setFeedbackMood(mood, btnId){
    const moodField = document.querySelector("#mood");

    moodField.value = mood;

    console.log(moodField.value);
}

window.onload = function() {
    var url = window.location.href;

    //search results link {AD SETTINGS}
    const resultLinks = document.querySelectorAll("a[data-ad-result=true]");
    const adBanner = document.querySelector("div[data-banner]");
    const body = document.querySelector("body");
    const closeAd = document.querySelector(".ad_close");
    const countdownElem = document.querySelector("#countdown");
    const continueLink = document.querySelector("a[data-continue=true]");
    const searchBtn = document.querySelector(".btn-search");

    resultLinks.forEach(links => {
       links.addEventListener('click', function(event){

            searchBtn.style.zIndex = 1;
            searchBtn.style.visibility = "hidden";
            searchBtn.style.opacity = "0";

            adBanner.style.display = "block";
            body.style.overflow = "hidden";
            const resultURL = links.dataset.href;
            event.preventDefault();

            //start counter
            countdown = 5;
            setInterval(function(){
                countdown = countdown - 1;   
                countdownElem.innerHTML = countdown;
                if(countdown < 1){
                    countdownElem.innerHTML = 0;
                    return;
                }
            }, 1000);

            //target link to continue button
            continueLink.href = resultURL;

            //redirect to result site
            var adRedirect = setTimeout(function(){
                    window.location.href = "https://www.lorveet.com/search"+resultURL;
            }, 5000);

            closeAd.addEventListener('click', function(event){
                adBanner.style.display = "none";
                body.style.overflowY = "scroll";
                searchBtn.style.display = "block";
                searchBtn.style.visibility = "unset";
                searchBtn.style.opacity = "1";
                event.preventDefault();
                clearTimeout(adRedirect);
                return;
            });

        }) 
    });

    //feedback controller
    const feedbackBtn = document.querySelector("#feedback");
    const feedModal = document.querySelector("div[data-modal-for=feedback]");
    const closeModal = document.querySelector("span[data-modal-close=feedback]");
    const closeNotification = document.querySelector("span[data-modal-close=welcome_profile]");
    const feedbackSubmit = document.querySelector("#submitFeedback");
    const userModal = document.querySelector("div[data-modal-for=welcome_profile]");

    feedbackBtn.addEventListener('click', function(event){      
        event.preventDefault();
        feedModal.style.opacity = "1";
        feedModal.style.display = "block";
        document.querySelector("body").style.overflowY = "hidden";
        document.querySelector("div.overlay").style.overflowY = "scroll";
    });

    closeModal.addEventListener('click', function(){
        feedModal.style.opacity = "0";
        feedModal.style.display = "none";
        document.querySelector("body").style.overflowY = "scroll";
    });

    // closeNotification.addEventListener('click', function(){
    //     userModal.style.opacity = "0";
    //     userModal.style.display = "none";
    //     document.querySelector("body").style.overflowY = "scroll";
    // });

    feedbackSubmit.addEventListener('click', function(){
        var feed_email = document.querySelector("#feedbackEmail").value;
        var feed_mood = document.querySelector("#mood").value;
        var feedback_msg = document.querySelector("#feedbackText").value;
        const currURL = window.location.href;

        $.ajax({
            url: "controller/feedbackController.php", 
            type: "POST",
            data: "feedback=true&email="+feed_email+"&mood="+feed_mood+"&feedbackText="+feedback_msg+"&url="+currURL,
            success: function(result){
                $("#feedbackResponse").html(result);
                document.querySelector("#feedbackEmail").value = "";
                document.querySelector("#feedbackText").value = "";
                setTimeout(function(){
                    feedModal.style.opacity = "0";
                    feedModal.style.display = "none";
                }, 3000);
            }
        });
    });

    //suggestion buttons controller
    var suggestButtons = document.querySelectorAll('button[data-additional-search-item]');

    suggestButtons.forEach( 
    function performSearch(btn){
        btn.addEventListener('click', function(){
            var url = window.location.href; 
            btnType = btn.dataset.query.split("=");
            btnName = btnType[0].substr(1, btnType[0].length);
            btnURL = btnType[1];
            // console.log(btnType + " " + btnName);
            if(url.indexOf("page") != "-1"){
                url = url.substring(0, url.indexOf("&page"));
            }
            console.log(btnType + " " + btnName);            
            window.location.href = updateQueryStringParameter(url, btnName, btnURL);
            // console.log(url);
        });
    });

    if((window.location.href).indexOf("locale") != "-1" || (window.location.href).indexOf("company") != "-1"){
        const filterBtn = document.querySelector('#filter-clear');
    
        filterBtn.addEventListener('click', function(event){
            console.log("Filters cleared.");
            
            url = filterBtn.dataset.action;
    
            window.location.href = url;
    
            event.preventDefault();
        });
    }

    //switch logo on mobile (only show on search page)
    var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    if(window.location.href.indexOf("search") != "-1" && width < 500){
        var navArea = document.querySelector(".nav-area");
            navArea.style.height = "5px";
    }
}

// const oldResults = document.querySelectorAll('.old-results');

// oldResults.forEach(result => result.addEventListener('mouseover', function(){
//     const sn = result.dataset.sn;
//     const toolTip = document.querySelector("#tooltip-parent-"+sn);
//     toolTip.style.display = "block";
// }));

// oldResults.forEach(result => result.addEventListener('mouseout', function(){
//     const sn = result.dataset.sn;
//     const toolTip = document.querySelector("#tooltip-parent-"+sn);
//     toolTip.style.display = "none";
// }));

const peopleGrid = document.querySelector("div[data-name=people-list]");

const genieLoader = document.querySelector("center[data-name=genie-loader]");

if(genieLoader !== null){
    setTimeout(function(){
        genieLoader.style.display = "none";
        peopleGrid.style.display = "block";
    }, 5000);
}

/**
 * Create Username with Ajax
 */

//  const typefield = document.querySelector("#create_username");

//  typefield.addEventListener('keyup', function(){
//         $.ajax({
//             url: "../../controller/userController.php", 
//             type: "POST",
//             data: "createUsername="+typefield.value,
//             success: function(result){
//                 // $().html(result);
//                 console.log(result);
//             }
//         });
//         console.log(typefield.value);
// });


/**
 * Results for last search navigation
 */

 const navRightSlider = document.querySelector("a[data-slider-nav-right-home]");
 const navLeftSlider = document.querySelector("a[data-slider-nav-left-home]");
 const listSlider = document.querySelectorAll("li[data-jobList-home]");

 if(navLeftSlider && navRightSlider){
    navLeftSlider.addEventListener('click', function(){
        listSlider.forEach(function(listSlide){
            if(listSlide.style.right > "0px"){
                listSlide.style.right = parseInt(listSlide.style.right) - 80 + "px";
            }
        })
    })

    navRightSlider.addEventListener('click', function(){
        listSlider.forEach(function(listSlide){
            
            if(parseInt(listSlide.style.right) < 80){
                listSlide.style.right = "80px";
            }else{
                // console.log(listSlide.style.right + "inside");
                listSlide.style.right = parseInt(listSlide.style.right) + 80 + "px";
            }
        })
    });
}

const editProfileBtn = document.querySelectorAll("a[data-target-modal]");

editProfileBtn.forEach(function(editBtn){
    editBtn.addEventListener('click', function(event){
        const targetModal = document.querySelector("div[data-modal-for="+editBtn.dataset.targetModal+"]");
        targetModal.style.display = "block";
    });
});

// close any modal which is attached to the close btn
const closeModalBtn = document.querySelectorAll("span[data-modal-close]");
closeModalBtn.forEach(function(closeBtn){
    closeBtn.addEventListener('click', function(event){
        const targetModal = document.querySelector("div[data-modal-for="+closeBtn.dataset.modalClose+"]");
        targetModal.style.display = "none";
    });
});

window.onload = function() {
    // var urlCaret = document.querySelectorAll("a[data-caret-child]");

    // urlCaret.forEach(function(caret){
    //     caret.addEventListener('click', function(event){
    //         event.preventDefault();
    //         var targetUrl = document.querySelector("#lrvt-"+caret.dataset.caretChild);
    //         console.log(targetUrl);
    //         targetUrl.css.visibility = "visible";

    //         document.querySelector("#lrvt-"+caret.dataset.caretChild).css.visibility = "visible";
    //     })
    // })
}


function openLrvt(id){
    // id.preventDefault();
    const lrvtUrl = document.querySelector("#lrvt-"+id);
    lrvtUrl.css.display = "initial";
}