-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2018 at 06:11 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lorveet`
--

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `sn` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`sn`, `user_id`, `content`, `dateCreated`) VALUES
(5, 'c1d4b99', '<div style=\"margin: 0 10%;\">\r\n    <div style=\"width: 100%; display: block; margin-bottom: 10px; float: left; border-bottom: 1px solid #00297d;\">\r\n        <img src=\"https://www.lorveet.com/assets/img/lorveet_logo.png\" style=\"float: left;position:relative;height: 50px;\" height=\"50\" alt=\"\">\r\n        <p style=\"float: right;width: 80%;font-size: 20px;margin-bottom:  0;text-align:  right;position:  relative;color: #00297d;\">Password reset request</p>\r\n    </div>\r\n    <div style=\"width: 100%; display: block; float: left;\">\r\n        <p style=\"color: #072e80;font-weight:  600;font-size: 16px;margin-bottom: 15px;\">Dear Olaegbe</p>\r\n        <p style=\"font-size: 23px; color: #4c4b4b; margin-bottom: 0px;\">\r\n            Here is a link to reset your password. This link expires in 5 minutes.\r\n        </p>\r\n    </div>\r\n    <div style=\"width: 100%; display: block; float: left;\">\r\n        <p>\r\n            Click this link to reset your password: https://www.lorveet.com/auth/reset-password?activate=5a8a0952041fd52ad7f3c28cdbfdc8a4\r\n        </p>\r\n    </div>\r\n    <div style=\"width: 100%; display: block; margin-top: 15px; float: left;\">\r\n        <i>This mail was sent automatically from lorveet.com and intended for the email address mentioned above. \r\n            If you receive this mail by mistake, kindly delete it. Thanks.</i>\r\n    </div>\r\n</div>', '2018-07-02 16:03:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`sn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `sn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
