<?php 
        if(is_array($job->getLastResults())){
            $Lastresults = $job->getLastResults();
    ?>
        <div class="full-sect" style="padding-top: 0 !important;">
            <p class="bold_1x">
                <?php echo $Lastresults['new_results_count']." new results for ".$Lastresults['keyword']." since you last searched."; ?>
            </p>
        </div>
        <div class="slider hr-slider">
            <a href="#" data-slider-nav-left-home class="fas fa-angle-left nav-left"></a>
            <ul>
            <?php 
                    foreach($job->getLastResults() as $newResults){
            ?>
                <li class="list-links" data-jobList-home>
                    <a target="_blank" class="resultSearch" href="<?php echo $newResults['sn']; ?>">
                        <?php echo $newResults['job_title']; ?>
                    </a>
                </li>
            <?php 
                    }
            ?>
            </ul>
            <a href="#" data-slider-nav-right-home class="fas fa-angle-right nav-right"></a>
        </div>
<?php } ?>