<div class="search-area">
    <?php include "controller/searchController.php"; ?>        
    <h3 class="search-title">Your job search begins here</h3>
    <i class="fas fa-search"></i>
    <form action="" method="post">
        <input type="text" placeholder="Type your search keyword and click the genie" name="job-search" id="search-home" autocomplete="off" class="search-form">
        <span id="cancel-search">&times;</span>
        <button class="btn-search" type="submit" id="submit-search-btn">
            <img src="assets/img/lorveetgenie.png" height="200px" alt="" srcset="">
        </button>
    </form>

    <div class="full-sect" style="position: relative; top: -50px;">
        <center>
            <ul class="list-items-hr">
                <li class="list-items">
                    <a href="<?php echo $genie->getUrl(); ?>">Jobs</a>
                </li>
                <li class="list-items">
                    <a href="<?php echo $genie->getUrl(); ?>genies">Genies</a>
                </li>
                <li class="list-items">
                    <a href="http://blog.lorveet.com">Blog</a>
                </li>
                <li class="list-items">
                    <a href="<?php echo $genie->getUrl(); ?>advertise">Advertise</a>
                </li>
            </ul>
        </center>
    </div>

    <?php 

        if( strstr($_SERVER['PHP_SELF'], "genies") ){
            include "includes/home_best_genies.php"; 
        }else{
            include "includes/home_new_results.php"; 
        }
    
    ?>

    
</div>