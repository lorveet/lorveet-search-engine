
    <div class="full-sect">
        <h3>
        #lorveet #genies are ordinary people who are doing the extra-ordinary things. Click on any of the images below to learn more about a Genie. 
        You can also search for Genies by typing a keyword in the search field above. 
        </h3>
    </div>
    
    <center data-name="genie-loader">
        <img src="<?php echo $genie->asset('assets/img/lorveetgenie.png'); ?>" class="genie_loader" alt="">
    </center>

    <div class="container" data-name="people-list" style="display: none; padding: 0 !important;">
        <?php 
            $bestGenies = $user->getRandomGenies();
            if(is_array($bestGenies)){
                foreach($bestGenies as $bestGeny){
        ?>
                <div class="people-grid">
                    <a href="<?php echo $genie->getUrl()?>people/profile/<?php echo $bestGeny['email']; ?>" class="people-link"></a>
                    <div class="people-up" style="background-image: url(<?php echo $genie->asset($bestGeny['picture']); ?>);"></div>
                    <div class="people-down">
                        <h4 class="people-title">
                            <?php echo $bestGeny['job_title']; ?>
                        </h4>
                        <p class="people-desc">
                            <?php echo substr($bestGeny['bio'], 0, 50); ?>
                        </p>
                    </div>
                </div>
        <?php 
                }
            }
        ?>
        <div class="full-sect">
            <center>
                <a href="<?php echo $genie->asset('people/signup/social'); ?>" class="btn">Become a genie</a>
            </center>
        </div>
    </div>