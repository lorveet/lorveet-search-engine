<div class="search-area tpad">
    <div class="container steps">
        <div class="col12 social_sign_up">
            <div class="step1">
                <span class="bg-text">1</span>
                <p>Sign-up with any one of your social media accounts below</p>
                <div class="col12 social_login_container">
                    <center>
                        <a target="_blank" href="https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=778bwhadd7oxyy&redirect_uri=https://www.lorveet.com/genies/people/verify/lnkdn/&state=<?php echo $genie->csrf(); ?>" class="lnkdin fab fa-linkedin-in">
                        </a>
                        <a target="_blank" href="http://" class="fcbk fab fa-facebook-f">
                        </a>
                        <a target="_blank" href="http://" class="twtt fab fa-twitter">
                        </a>
                    </center>
                </div>
            </div>
            <div class="step2">
                <span class="bg-text">2</span>
                <p>Create your bio</p>
            </div>
            <div class="step2">
                <span class="bg-text">3</span>
                <p>All done! <i class="fal fa-laugh-beam"></i></p>
            </div>
        </div>
        <div class="col12 full-sect">
            <center>
                <a href="<?php echo $genie->asset('https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=778bwhadd7oxyy&redirect_uri=https://www.lorveet.com/genies/people/verify/lnkdn/&state='.$genie->csrf()); ?>" class="btn">Join now</a>
            </center>
        </div>
    </div>
</div>