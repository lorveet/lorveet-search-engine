<?php 
    //user controller
    // include "controller/userController.php";

    //this file is only called when we have $_GET['profile']
    $people = $user->getProfile($_GET['profile']);
    $fullname = $people['firstname'].' '.$people['lastname'];
    //include modal file
    include "modals.php";
?>

<div class="container" style="margin-top: 0 !important;">
    <div class="profile-header">
        <div class="person-details">
            <h2><?php echo $fullname; ?></h2>
            <h6><?php echo $people['job_title']; ?></h6>
            <span><?php echo $user->print($people['state'], "Nigeria"); ?></span>
        </div>
        <div class="profile-picture">
            <img src="<?php echo $genie->asset($people['picture']); ?>" alt="">
        </div>
    </div>
    <div class="people_profile">
        <div class="detail-sect">
            <div class="col12 profile_sect_header">
                <h2 class="sect_header pull-left">Personal Details</h2>
                <a href="#" data-target-modal="personal_details" class="pull-right">Edit</a>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Fullname</b>
                    <span class="detail-list pull-right"><?php echo $fullname; ?></span>
                </div>
                <div class="col6"></div>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Email</b>
                    <span class="detail-list pull-right"><?php echo $user->print($people['email'], ""); ?></span>
                </div>
                <div class="col6"></div>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Username</b>
                    <span class="detail-list pull-right"><?php echo $user->print($people['username'], ""); ?></span>
                </div>
                <div class="col6"></div>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Phone</b>
                    <span class="detail-list pull-right"><?php echo $user->print($people['phone'], "No phone number"); ?></span>
                </div>
                <div class="col6"></div>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Date of Birth</b>
                    <span class="detail-list pull-right"><?php echo $user->print($people['dob'], "No date added"); ?></span>
                </div>
                <div class="col6"></div>
            </div>
        </div>
        <div class="detail-sect">
            <div class="col12 profile_sect_header">
                <h2 class="sect_header pull-left">About</h2>
                <a href="#" data-target-modal="bio" class="pull-right">Edit</a>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Biography</b>
                    <span class="detail-list pull-right">
                        <?php echo $user->print($people['bio'], "This user has not provided a bio"); ?>
                    </span>
                </div>
                <div class="col6"></div>
            </div>
        </div>
        <div class="detail-sect">
            <div class="col12 profile_sect_header">
                <h2 class="sect_header pull-left">Social &amp; Portfolio</h2>
                <a href="#" data-target-modal="social_details" class="pull-right">Edit</a>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Facebook</b>
                    <span class="detail-list pull-right">
                        <a href="#">facebook.com/sam</a>
                    </span>
                </div>
                <div class="col6"></div>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">Instagram</b>
                    <span class="detail-list pull-right">
                        <a href="#">facebook.com/sam</a>
                    </span>
                </div>
                <div class="col6"></div>
            </div>
            <div class="col12">
                <div class="col6">
                    <b class="pull-left">LinkedIn</b>
                    <span class="detail-list pull-right">
                        <a href="#">facebook.com/sam</a>
                    </span>
                </div>
                <div class="col6"></div>
            </div>
            
        </div>

        <div class="detail-sect">
            <div class="col12 profile_sect_header">
                <h2 class="sect_header pull-left">Tags (Keywords)</h2>
                <a href="#" class="pull-right">Edit</a>
            </div>
            <div class="col12">
                    <span class="detail-list pull-left">
                        These are the keywords that are relevant to your profession.
                    </span>
            </div>
            <div class="col12">
                    <span class="detail-list pull-left">
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                        <a href="#">#engineer</a>
                    </span>
            </div>            
        </div>
    </div>
</div>