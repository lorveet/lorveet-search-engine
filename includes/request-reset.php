<h4 class="title">Reset password</h4>
    <div class="signup-sect">
    <form action="" id="signup" method="post">
        <div class="form-container">
            <div class="form-group">
                <label for="email" class="form_half">Enter your email</label>
                <input type="text" required name="email" id="email" placeholder="you@email.com" class="form_c form_full">
            </div>    
            <p class="login-msc">
                If your email is correct, we will send you a link to reset your password.
                <a href="signup">Sign up instead?</a> &nbsp; <a href="signup">Sign in</a>
            </p>
            <div class="form-group">
                <button id="submitForm" name="reset-password" class="pull-right btn">Sign in</button>
            </div>
        </div>
    </form>
    </div>