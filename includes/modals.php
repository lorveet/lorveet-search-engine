<!-- // Modal for updating personal information -->
<div class="overlay" data-modal-for="personal_details">
    <div class="modal modal-feedback">
        <div class="modal-body">
            <span class="close pull-right" title="Close" data-modal-close="personal_details">X</span>
            <div class="col12">
                <center>
                    <img height="50" src="<?php echo $genie->asset("assets/img/lorveet_logo.png"); ?>" alt="">
                </center>
            </div>
            <h4 class="profile-modal-title" style="">Edit your personal details</h4>
            <form action="" method="post">
                <div class="form-group-half pull-left">
                    <label for="firstname" class="form_half bold_1x">First name</label>
                    <input type="text" required name="firstname" id="firstname" value="<?php echo $people['firstname']; ?>" class="form_c form_half">
                </div>    
                <div class="form-group-half pull-right">    
                    <label for="lastname" class="form_half bold_1x">Last name</label>
                    <input type="text" required name="lastname" id="lastname" value="<?php echo $people['lastname']; ?>" class="form_c form_half">
                </div>

                <div class="form-group">
                    <label for="username" class="center_text bold_1x">
                        Username
                    </label>

                    <input type="text" required name="username" id="username" value="<?php echo $people['username']; ?>" class="form_c form_half">
                </div>

                <div class="form-group-half pull-right">    
                    <label for="phone" class="center_text bold_1x">
                        Phone
                    </label>
                    <input type="tel" required name="phone" id="phone" value="<?php echo $people['phone']; ?>" class="form_c form_half">
                </div>
                <div class="form-group-half pull-left">
                    <label for="dob" class="center_text bold_1x">
                        Date of birth
                    </label>
                    <input type="date" required name="dob" id="dob" value="<?php echo $people['dob']; ?>" class="form_c form_half">
                </div>
                <div class="form-group">
                    <button id="submitForm" name="updatePersonalInfo" class="pull-right btn">Sign up</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- // Modal for updating biography information -->
<div class="overlay" data-modal-for="bio">
    <div class="modal modal-feedback">
        <div class="modal-body">
            <span class="close pull-right" title="Close" data-modal-close="bio">x</span>
            <div class="col12">
                <center>
                    <img height="50" src="<?php echo $genie->asset("assets/img/lorveet_logo.png"); ?>" alt="">
                </center>
            </div>
            <h4 class="profile-modal-title" style="">Edit your bio</h4>
            <form action="" method="post">
                <div class="form-group">
                    <label for="bio" class="bold_1x">Bio</label>
                    <small class="muted-text">300 max characters</small>
                    <textarea required name="bio" id="bio" class="form_c textarea form_full"><?php echo $people['bio']; ?></textarea>
                </div>    
                <div class="form-group">
                    <button id="submitForm" name="updateBio" class="pull-right btn">Sign up</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- // Modal for updating social information -->
<div class="overlay" data-modal-for="social_details">
    <div class="modal modal-feedback">
        <div class="modal-body">
            <span class="close pull-right" title="Close" data-modal-close="social_details">x</span>
            <div class="col12">
                <center>
                    <img height="50" src="<?php echo $genie->asset("assets/img/lorveet_logo.png"); ?>" alt="">
                </center>
            </div>
            <h4 class="profile-modal-title" style="">Edit your social links</h4>
            <form action="" method="post">
                <div class="form-group">
                    <label for="Facebook" class="bold_1x">Facebook</label>
                    <input type="text" required name="facebook" id="Facebook" placeholder="https://www.facebook.com/lorveet" class="form_c form_half">
                </div>    

                <div class="form-group">    
                    <label for="lastname" class="bold_1x">Instagram</label>
                    <input type="text" required name="instagram" id="Instagram" placeholder="https://www.instagram.com/lorveet" class="form_c form_half">
                </div>

                <div class="form-group">
                    <label for="linkedin" class="center_text bold_1x">
                        Linkedin
                    </label>
                    <input type="text" required name="linkedin" id="linkedin" placeholder="https://www.linkedin.com/company/lorveet" class="form_c form_half">
                </div>

                <div class="form-group">
                    <button id="submitForm" name="updateLinks" class="pull-right btn">Sign up</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php 
    if(isset($message)){
?>
    <div class="overlay" data-modal-for="notification_user_settings" style='display:block;'>
    <div class="modal modal-feedback">
        <div class="modal-body">
            <span class="close pull-right" title="Close" data-modal-close="notification_user_settings">x</span>
            <div class="col12">
                <center>
                    <img height="50" src="<?php echo $genie->asset("assets/img/lorveet_logo.png"); ?>" alt="">
                </center>
            </div>
            <h4 class="profile-modal-title" style=""><?php echo $message['header']; ?></h4>
                <div class="form-group">
                    <center>
                        <label for="text" class="center_text bold_1x">
                            <?php echo $message['body']; ?>
                        </label>
                    </center>
                </div>
        </div>
        </div>
    </div>
<?php 
    }
?>

<?php
    if(isset($_GET['status']) && $_GET['status'] === "welcome"){
?>
    <div class="overlay" data-modal-for="welcome_profile" style='display:block;'>
        <div class="modal modal-feedback">
            <div class="modal-body">
                <!-- <span class="close pull-right" title="Close" data-modal-close="welcome_profile">X</span> -->
                <div class="col12">
                    <center>
                        <img height="50" src="<?php echo $genie->asset("assets/img/lorveet_logo.png"); ?>" alt="">
                    </center>
                </div>
                <h4 class="profile-modal-title" style="">Congratulations on becoming a Lorveet Genie.</h4>
                    <div class="form-group">
                        <center>
                            <label for="text" class="center_text bold_1x">
                                Your Lorveet Genie address is: <a href="#">genie.lorveet.com/<?php echo $people['username'] ?></a>
                            </label>
                        </center>
                    </div>
                    <br>
                    <div class="col12">
                        <p class="space">In order to increase your Genie ranking – that is your position when people search for people with your skills 
                                    – you will need to get people to endorse your profile. You can do this by using the tools below: </p>
                        <ol>
                            <li>
                                 Share your profile on social media 
                            </li>
                            <li>
                                Download and print your person Genie sign. You can post this sign in the location of your business for your clients to see.
                            </li>
                            <li>
                                Download your Lorveet Genie email signature to add to your email template.
                            </li>
                        </ol>
                    </div>

            </div>
        </div>
    </div>
<?php 
    }
?>