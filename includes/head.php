<?php 
    ob_start(); session_start(); 
    if(isset($_SESSION['loggedIn'])){
        $sessionid = $_SESSION['loggedIn'];
    }else{
        $sessionid = "";
    }

    include "controller/genie.class.php";
    $genie = new genie();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116161255-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-116161255-2');
    </script>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Lorveet is an employment agency based in Lagos. We run a search engine focused on helping Nigerians get the job they desire.">
    <title>Lorveet Genie - <?php echo isset($_GET['q']) ? ucfirst($_GET['q'])." jobs on Lorveet" : "Job Search Engine"; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" media="screen and (min-device-width: 100px)" href="<?php echo $genie->asset('assets/css/main-mobile.css') ?>">
    <link rel="stylesheet" type="text/css" media="screen and (min-width: 768px)" href="<?php echo $genie->asset('assets/css/main.css') ?>" />
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:400|Amaranth|Chivo|Signika:700|Cabin+Condensed:700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="shortcut icon" href="<?php echo $genie->asset('assets/img/lorveetgenie.png'); ?>" type="image/x-icon">
</head>