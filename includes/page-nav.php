<?php require_once "controller/userController.php"; ?>
<div class="nav-area">
    <div class="brand-area display-large">
        <!-- Logo here -->
        <center>
        <a href="<?php echo $genie->getUrl(); ?>">
            <img src="<?php echo $genie->asset('assets/img/lorveet_logo.png'); ?>" alt="" srcset="">
        </a>
        </center>
    </div>
    <div class="nav-sub">
        <center>
        <div class="nav-pages display-large">
            <ul class="center">
                <li><a href="<?php echo $genie->asset('login') ?>">Sign in</a></li>
                <li>:</li>
                <li><a href="<?php echo $genie->asset('signup') ?>">Sign Up</a></li>
            </ul>
        </div>
        </center>
    </div>
</div>