<?php 
    include "includes/head.php";
    include "includes/page-nav.php";
?>

<body>
    <div class="main-content">
        <div class="center-page">
            <?php if(isset($error) && is_array($error)): ?>
            <div class="alert-<?php echo $error['type'] ?>">
                <ul>
            <?php foreach($error['message'] as $err): ?>
                    <li class="alert-list"><?php echo $err; ?></li>
            <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <?php 
                if(isset($_GET['activate'])):
                    include "includes/reset-pwd.php";
                else:
                    include "includes/request-reset.php";
                endif;
            ?>
        </div>
    </div>
    <?php include "includes/footer.php"; ?>
</body>