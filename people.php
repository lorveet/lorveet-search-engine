<?php include "includes/head.php"; ?>
<body>
    <?php include "includes/page-nav.php"; ?>
    <?php 
        if(isset($_GET['signup']) && $_GET['signup'] == "social"){
            include "includes/signup_social.php";
        }elseif(isset($_GET['verify']) && $_GET['verify'] == "lnkdn"){
            include "includes/auth_linkedin.php";
        }elseif(isset($_GET['auth']) && $_GET['auth'] == "lnkdn"){
            print_r($_GET);
        }elseif(isset($_GET['profile'])){
            include "includes/people_profile.php";
        }
    ?>
</body>
    <?php include "includes/footer.php"; ?>
</html>
