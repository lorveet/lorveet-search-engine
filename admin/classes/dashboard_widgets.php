<?php 

    class Dashboard{

        public function registeredUsers($connection, $num_rows = false, $limit = false){
            /*
                this class will generate the facts on the home
                page like users registered, votes, highest votes,
                etc.

                @returns: array if num_rows = false else returns int
            */

                $query = "SELECT * FROM visitors";
                $query .= isset($limit) & $limit != "" ? " LIMIT $limit" : '';

            if($num_rows){
                return $connection->query($query)->num_rows;
            }else{
                $result = $connection->query($query);
                    if($result->num_rows == 0){
                        return "No visitors registered";
                    }
                    
                    $result_array = array();

                    while($results = $result->fetch_array()){

                        $result_array[] = $results;

                    }

                        return $result_array;
            }
        }


        public function totalVotes($connection, $num_rows = false){
            /*
                this class will generate the facts on the home
                page like users registered, votes, highest votes,
                etc.

                @returns: array if num_rows = false else returns int
            */

                $query = "SELECT * FROM logo";

            if($num_rows){
                return $connection->query($query)->num_rows;
            }else{
                $result = $connection->query($query);
                    if($result->num_rows == 0){
                        return "No votes yet";
                    }
                    
                    while($results = $result->fetch_array()){

                        return $results;

                    }
            }
        }

        public function highestVotes($connection, $num_rows = false, $logoName){
            /*
                this class will generate the facts on the home
                page like users registered, votes, highest votes,
                etc.

                @returns: array if num_rows = false else returns int
            */

                $query = "SELECT * FROM logo WHERE logo_name = '$logoName' ";
                
            if($num_rows){
                return $connection->query($query)->num_rows;
            }else{
                $result = $connection->query($query);
                    if($result->num_rows == 0){
                        return "No votes yet";
                    }
                    
                    while($results = $result->fetch_array()){

                        return $results;

                    }
            }
        }

        public function fetchVotes($connection, $num_rows = false){
            /*
                this class will generate the facts on the home
                page like users registered, votes, highest votes,
                etc.

                @returns: array if num_rows = false else returns int
            */

                $query = "SELECT DISTINCT logo_name FROM logo";
                
            if($num_rows){
                return $connection->query($query)->num_rows;
            }else{
                $result = $connection->query($query);
                if($result->num_rows == 0){
                    return "No visitors registered";
                }
                
                $result_array = array();

                while($results = $result->fetch_array()){

                    $result_array[] = $results;

                }

                    return $result_array;
            }
        }




    }

// SELECT SUM(votes) FROM logo WHERE logo_name = 'logo1';
?>