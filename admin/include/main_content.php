<div class="container-fluid" style="padding-top: 20px">
<h2 style="display: inline;">Dashboard</h2>
<hr>

<?php 

    include "connection.php";
    include "classes/dashboard_widgets.php";

    $dashboard = new Dashboard();



?>


    <div class="container">
        <!-- BEGIN STORE SUMMARY -->
        <div class="col-lg-12 store-summary" style="float: left;">
            <h2 class="store-headline">STORE OVERVIEW</h2>
            <div class="col-md-4 pull-left">
                <h2 class="fore-front-text">
                    <?php echo $dashboard->registeredUsers($connection, true); ?>
                </h2>
                <small>TOTAL REGISTERED EMAILS</small>
            </div>
            <div class="col-md-4 pull-left">
                <h2 class="fore-front-text">
                    <?php echo $dashboard->totalVotes($connection, true); ?>
                </h2>
                <small>TOTAL VOTES</small>
            </div>
            <div class="col-md-4 pull-left">
                <h2 class="fore-front-text">
                    <?php echo max(
                            $dashboard->highestVotes($connection, true, "logo1"),
                            $dashboard->highestVotes($connection, true, "logo2"),
                            $dashboard->highestVotes($connection, true, "logo3")
                        ); ?>
                </h2>
                <small>LOGO WITH HIGHEST VOTES</small>
            </div>
        </div>
        <!-- END STORE SUMMARY -->

        <!-- REGISTERED EMAILS -->
        <div class="col-lg-12 products-summary" style="float: left;">
            <h2 class="store-headline">REGISTERED EMAILS</h2>
            <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr class="no-border">
                    <th scope="col">S/N</th>
                    <th scope="col">Email</th>
                    <th scope="col">Date Registered</th>
                    <th scope="col">IP Address</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $emailData = $dashboard->registeredUsers($connection, false, 10);
                    if($emailData == 0): echo "No rows found";
                    else:
                    foreach($emailData as $eachData):
                ?>
                    <tr>
                    <th scope="row"><?php echo $eachData['id']; ?></th>
                    <td><?php echo $eachData['email']; ?></td>
                    <td><?php echo $eachData['registered_date']; ?></td>
                    <td><?php echo $eachData['ip']; ?></td>   
                    </tr>
                    <?php 
                        endforeach; 
                    endif;
                    ?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- END REGISTERED EMAILS -->



        <!-- CONVERSION RATE CHART -->
        <div class="col-lg-12 conversion-summary">
            <h2 class="store-headline">LOGO VOTES</h2>
            <canvas id="myChart" width="1000" height="400"></canvas>
        </div>
        <!-- END CONVERSION GRAPH -->

    </div>
</div>
