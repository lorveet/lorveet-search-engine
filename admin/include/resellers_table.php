<?php 
    include "connection.php";
    include "resellers_func.php";
?>

<h2 style="display: inline;">Resellers</h2>
<hr>
        <!-- REGISTERED EMAILS -->
        <div class="col-lg-12 products-summary" style="float: left;">
            <h2 class="store-headline">REGISTERED EMAILS</h2>
            <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr class="no-border">
                    <th scope="col">S/N</th>
                    <th scope="col">Fullname</th>
                    <th scope="col">Profile URL</th>
                    <th scope="col">Instagram Username</th>                    
                    <th scope="col">Followers</th>
                    <th scope="col">Date Registered</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $sn = 1;

                    $followers = $connection->query("SELECT * FROM resellers");
                    if($followers->num_rows != 0){
                        while($follower = $followers->fetch_assoc()):
                ?>
                    <tr>
                    <th scope="row"><?php echo $sn++; ?></th>
                    <td><?php echo $follower['firstname']." ".$follower['lastname'];; ?></td>
                    <td><?php echo "<a href='https://www.instagram.com/".$follower['instagram_handle']."'>".$follower['instagram_handle']."</a>"; ?></td>
                    <td><?php echo $follower['instagram_handle']; ?></td>   
                    <td><?php echo getFollowersCount($follower['instagram_handle']); ?></td>   
                    <td><?php echo $follower['date_registered']; ?></td>
                    </tr>
                    <?php 
                        endwhile;
                    }else{
                            echo "<tr>
                                    <td>0</td>
                                    <td colspan='3'>No rows found</td>
                                    
                                </tr>";
                    }
                    ?>
                </tbody>
            </table>
            </div>
        </div>