<?php 

    include "connection.php";
    include "classes/dashboard_widgets.php";

    $dashboard = new Dashboard();

?>

<h2 style="display: inline;">Registered Users (<?php echo $dashboard->registeredUsers($connection, true, false); ?>)</h2>
<hr>

        <!-- REGISTERED EMAILS -->
        <div class="col-lg-12 products-summary" style="float: left;">
            <h2 class="store-headline">REGISTERED EMAILS</h2>
            <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr class="no-border">
                    <th scope="col">S/N</th>
                    <th scope="col">Email</th>
                    <th scope="col">Date Registered</th>
                    <th scope="col">IP Address</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $emailData = $dashboard->registeredUsers($connection, false, false);
                    if($emailData != 0){
                    foreach($emailData as $eachData):
                ?>
                    <tr>
                    <th scope="row"><?php echo $eachData['id']; ?></th>
                    <td><?php echo $eachData['email']; ?></td>
                    <td><?php echo $eachData['registered_date']; ?></td>
                    <td><?php echo $eachData['ip']; ?></td>   
                    </tr>
                    <?php 
                        endforeach;
                        }else{
                            echo "<tr>
                                    <td>0</td>
                                    <td colspan='3'>No rows found</td>
                                    
                                </tr>";
                        }
                     ?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- END REGISTERED EMAILS -->