<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" style="background-color: #9fc538!important">

<a class="navbar-brand" href="index.php">
  LORVEET
</a>

<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>


<div class="collapse navbar-collapse" id="navbarsExampleDefault">
  <ul class="navbar-nav mr-auto">
            <!-- <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#">Disabled</a>
            </li> -->
  </ul>

  <div class="logout">
    <a href="#logout" title="Logout" class="nav-settings">
      <i class="fas fa-sign-out-alt"></i>
    </a>
  </div>
</div>

</nav>
