<h2>VOTES</h2>
<hr/>

<style>
    .col-md-2{
        display: inline-block;
        float: right;        
    }

    .product-name{
        line-height: 42px;
    }
</style>
    <?php 
 
        include "connection.php";
        
        include "classes/dashboard_widgets.php";

        $dashboard = new Dashboard();

        $voteData = $dashboard->fetchVotes($connection, false);

        if($voteData != 0){
        foreach($voteData as $eachVote):
            if($eachVote['logo_name'] == "undefined") return;
    ?>
    <div class="col-lg-10 ratings-card">

        <div class="col-md-3 inline-display">
            <div class="product-image">
                <img src="../img/<?php echo $eachVote['logo_name']; ?>.png" class="img-thumbnail">
            </div>
        </div>

        <div class="col-md-5 inline-display">
            <h3 class="product-name"><?php echo ucfirst($eachVote['logo_name']); ?></h3>
        </div>
        <div class="col-md-2">
            <h6 class="product-name">
                NO. OF VOTES: <?php echo $dashboard->highestVotes($connection, true, $eachVote['logo_name']); ?>
            </h6>
        </div>
    </div>

    <?php endforeach;}else{ echo "<h2>No votes recorded</h2>"; } ?>
