<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar-css.css">    
    <link rel="stylesheet" href="css/custom.css">   
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> 
    <!-- <script src="js/bootstrap.min.js" type="text/javascript"></script> -->
    <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <title>Lorveet Admin</title>
</head>
<body>
    <?php include "include/top-nav.php"; ?>
    <?php include "include/sidebar.php"; ?>
    <!-- Page Content -->
    <div id="page-content-wrapper">