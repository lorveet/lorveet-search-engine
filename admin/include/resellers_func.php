<?php 

function getFollowersCount($instagram_handle){
    $response = file_get_contents("https://www.instagram.com/web/search/topsearch/?query=$instagram_handle");
    if ($response !== false) {
        $data = json_decode($response, true);
        if ($data !== null) {

            foreach($data['users'] as $user){
                if($instagram_handle == $user['user']['username']){
                    $givenUser = $user['user']['username'];
                    $followers_count = $user['user']['follower_count'];
                    break;
                }
            }

            return $followers_count;
        }
    }
}