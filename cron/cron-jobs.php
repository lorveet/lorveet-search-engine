<?php 
    include $_SERVER['DOCUMENT_ROOT']."/lorveet/controller/connection.php";
    include $_SERVER['DOCUMENT_ROOT']."/lorveet/controller/genie.class.php";

    $xml = file_get_contents("https://ngcareers.com/xmlfeed.xml");
    $jobs = new SimpleXMLElement($xml);

    $db = new DBConnection();

    $genie = new genie();

    $totalJobs = count($jobs->jobs->job);

    for($i = 0; $i < count($jobs->jobs->job); $i++){
        $title = $jobs->jobs->job[$i]->title;
        $url = $jobs->jobs->job[$i]->url;
        $salary = $jobs->jobs->job[$i]->salary;
        $category = $jobs->jobs->job[$i]->category;
        $company = $jobs->jobs->job[$i]->company;
        $date = date("Y-m-d", strtotime($jobs->jobs->job[$i]->date));
        $experience = $jobs->jobs->job[$i]->experience;
        $location = $jobs->jobs->job[$i]->state;
        $type = $jobs->jobs->job[$i]->jobtype;
        $specialization = $jobs->jobs->job[$i]->category;
        $description = $jobs->jobs->job[$i]->description;
        $experience = $jobs->jobs->job[$i]->experience." experience";

        // $queryStatus[] = $db->execQuery("INSERT INTO jobs(job_title, company, job_link, job_location, 
        //                             job_industry, date_posted, job_requirements, 
        //                             job_time, salary) 
        //                 VALUES ('$title', '$company', '$url', '$location', '$specialization',
        //                         '$date', '$experience', '$type', '$salary')");
        $jobNode = '<div>
                        <h3 style="margin-bottom: 5px;">
                            <a href="#" style="color: rgb(0, 4, 126);text-decoration: none;">{job-title} at {company}</a>
                        </h3>
                        <b>Minimum requirements:</b> {requirements}
                        <b>Job type:</b> {job-type}
                        <span style="color: #737373; font-weight: 600;width: 100%;font-size: 14px;display: block;">Industry: <a href="https://www.lorveet.com/search?q={industry}&src=industry_email_feed">{industry}</a></span>
                        <span style="color: #737373; font-weight: 600;width: 100%;font-size: 14px;display: block;">Location: <a href="https://www.lorveet.com/search?q={location}&src=location_email_feed">{location}</a></span>
                        <span style="color: #737373; font-weight: 600;width: 100%;font-size: 14px;display: block;">Date: {date}</span>                   
                    </div>';

        $jobNode = str_replace("{job-title}", $title, $jobNode);
        $jobNode = str_replace("{company}", $company, $jobNode);
        $jobNode = str_replace("{requirements}", $experience, $jobNode);
        $jobNode = str_replace("{job-type}", $type, $jobNode);
        $jobNode = str_replace("{industry}", $specialization, $jobNode);
        $jobNode = str_replace("{location}", $location, $jobNode);
        $jobNode = str_replace("{industry}", $specialization, $jobNode);
        $jobNode = str_replace("{date}", $date, $jobNode);

        $mailContent .= $jobNode;
    }

    $statusMsg = "";

    foreach($queryStatus as $status){
        $statusMsg .= $status."<br/>";
    }

    //send email 
    // $emailTemplate = file_get_contents("../../html/cron-notification.html");

    //subscribers email
    $emailTemplate_too = file_get_contents("../../html/job-listings.html");

    //subscribers content
    $emailTemplate_too = str_replace("{content}", $mailContent, $emailTemplate_too);

    echo $emailTemplate_too;

    // $emailTemplate = str_replace("{job-source}", "https://ngcareers.com/xmlfeed.xml", $emailTemplate);
    // $emailTemplate = str_replace("{num-jobs}", $totalJobs, $emailTemplate);
    // $emailTemplate = str_replace("{date-time}", date("Y-m-d"), $emailTemplate);
    // $emailTemplate = str_replace("{status}", $statusMsg, $emailTemplate);

    // $genie->EmailNotify("Cron Job run successful", $emailTemplate);
?>