<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        opacity: 0;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var map;

function initialize() {
  // Create a map centered in Pyrmont, Sydney (Australia).
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 9.0820, lng: 8.6753}, 
    zoom: 15
  });

  // Search for Google's office in Australia.
  var request = {
    location: map.getCenter(),
    radius: '50000',
    type: ['establishment'],
    fields: ['name', 'rating', 'formatted_phone_number', 'geometry', 'opening_hours', 'icon']
  };

  var service = new google.maps.places.PlacesService(map);
  service.textSearch(request, callback);
}

var resultJson;

// Checks that the PlacesServiceStatus is OK, and adds a marker
// using the place ID and location from the PlacesService.
function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
        var marker = new google.maps.Marker({
        map: map,
        place: {
            placeId: results[i].place_id,
            location: results[i].geometry.location
        }
        });

        resultJson = JSON.stringify(results);
        console.log(resultJson);
        
    }
        document.querySelector("#results").innerHTML = resultJson;
    }
}

// google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyOOeEkIak_7U2ddMEuLiNN9Dl-DPnuZ8&libraries=places&callback=initialize" async defer></script>
    <div id="map"></div>
    <div id="results"></div>
</body>
</html>